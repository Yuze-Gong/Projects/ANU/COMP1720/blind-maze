let heartbeat, rainTraffic, crowd
let sounds = []

function preload() {
   // load any assets (images, sounds etc.) here
   heartbeat = loadSound('assets/heartbeat.mp3')
   rainTraffic = loadSound('assets/rain and traffic.mp3')
   crowd = loadSound('assets/crowd noise.mp3')
   
   heartbeat.rate(0.4)
   heartbeat.setVolume(0.2)
   rainTraffic.setVolume(0.1)
   crowd.setVolume(0.05)

   sounds = [heartbeat, rainTraffic, crowd]
   // sounds = [heartbeat]
}

let map, transition

const fps = 60

function setup() {
   createCanvas(windowWidth, windowHeight);
   // put setup code here

   // setup framerate
   frameRate(fps)

   // init maze and maze functions
   map = genMap(color(255, 255, 255, 0.5*255))

   heart = genHeart()

   transition = genTransitionAnimation()

   // alpha value to simulate glowing
   // map.alpha = random(0.73*255, 0.8*255)
   map.alpha = 255
}

const maze_size = 15
const wall_color = 15
// const wall_color = 255

var scene = titleScene;

function draw() {
   scene()
}

const title = 'Heart Maze',
      subtitle = 'Click to Start',
      ending = 
"how's the journy of finding your heart? \
Our brains have been feeding various infos all the time, how long haven't you \
listened your heart?\ How's the journey of finding your heart with a long detour ? "

function titleScene() {
   background(0)
   textAlign(CENTER, CENTER)

   push();
      translate(map.tx, map.ty)
      map.printMaze();

      const x = heart.x * map.grid_length + map.grid_length / 2,
            y = heart.y * map.grid_length + map.grid_length / 3;
      transition.x = heart.x, transition.y = heart.y;
      heart.paintHeart(x, y, map.grid_length/2);
   pop();
   
   fill(255)
   textSize(width/10)
   text(title, width/2, height*0.4)

   var blink = floor(frameCount / fps) % 2
   if (blink != 0) {
      fill(255 * blink)
      textSize(width/30)
      text(subtitle, width/2, height*0.55)
   }
   
}

function mouseClicked() {
   if (scene == titleScene) {
      scene = mainScene;
      map.color = wall_color;
      heart = genHeart();
      transition = genTransitionAnimation();
      playSound();
   }
}

var heartbeatAmp, heartbeatCount = 0;
function heartbeatScene() {
   const duration = 6,
         haltDuration = 2,
         blackduration = 2,
         stopFrameNum = duration * fps,
         blackFrameNum = (duration + haltDuration) * fps,
         frameNum = (duration + haltDuration + blackduration) * fps;
   
   heartbeatCount += 1;
   mainScene();

   if (heartbeatCount >= stopFrameNum && heartbeatAmp.getLevel() == 0) 
      heartbeat.pause();

   if (heartbeatCount >= blackFrameNum)
      background(0);

   if (heartbeatCount > frameNum)
      scene = endScene;
}

function endScene() {
   fill(255)
   background(0)
   textSize(width/40)
   rectMode(CENTER)
   text(ending, width/2, height/2, width*0.8, height*0.8)
   // console.log(x, y, w, h)
   heartbeat.pause()
}

function mainScene() {
   // put drawing code here
   background(wall_color, map.alpha)
   // background(0)

   // move maze to the center of the canvas
   translate(map.tx, map.ty)

   heart.draw()

   // map.draw()
   if (transition.status)
      transition.draw()
   else
      map.printPlayer()

   map.printMaze()

   if (heart.x == map.x && heart.y == map.y) {
      scene = heartbeatScene;
      heartbeatAmp = new p5.Amplitude();
      heartbeatAmp.setInput(heartbeat);
   }
}

let heart

function genHeart() {

   var heart = {}

   // randomize heart's coordinate in grid
   heart.x = floor(random(maze_size - 1)), 
   heart.y = floor(random(maze_size - heart.x-1, maze_size - 1))

   // calculate the max possible euclidean distance between player and heart
   const maxX = max(heart.x, abs(maze_size - heart.x-1)),
         maxY = max(heart.y, abs(maze_size - heart.y-1)),
         maxDis = sqrt(maxX ** 2 + maxY ** 2)

   // const & var to achieve heartbeat effects
   const heartAmpPeak = heartbeat.getPeaks(1)[0]
   var heartAmp = new p5.Amplitude()
   heartAmp.setInput(heartbeat)

   // draw beating heart & update heart's amp based on the distance between player and heart
   heart.draw = function() {
      const x = heart.x * map.grid_length + map.grid_length / 2,
            y = heart.y * map.grid_length + map.grid_length / 3

      var heartPercentage = heartAmp.getLevel() / (heartAmpPeak * 0.8)
      var size = map.grid_length / 2 * constrain(heartPercentage, 0.95, 1)

      heart.paintHeart(x, y, size)
      heart.updateAmp()
   }

   // function borrowed from other p5 sketches, see reference for details
   heart.paintHeart = function(x, y, size) {
      
      if (transition.x == heart.x && transition.y == heart.y)
         fill('red')
      else
         fill(wall_color)
      
      beginShape();
         vertex(x, y);
         bezierVertex(x - size / 2, y - size / 2, x - size, y + size / 3, x, y + size);
         bezierVertex(x + size, y + size / 3, x + size / 2, y - size / 2, x, y);
      endShape(CLOSE);  
   }

   heart.updateAmp = function() {
      const x = abs(map.x - heart.x), 
            y = abs(map.y - heart.y),
            dis = sqrt(x ** 2 + y ** 2)
      
      // calculate ratio
      var r = p => pow(1-p, 1.2)
      
      // console.log(ratio)

      heartbeat.setVolume(lerp(0.01, 1, r(dis/maxDis)))
      heartbeat.rate(lerp(0.5, 1.5, r(dis/maxDis)))

      rainTraffic.setVolume(lerp(0.1, 0, r(y/maxY)))
      crowd.setVolume(lerp(0.05, 0, r(x/maxX)))
   }
   
   // map.x = heart.x
   // map.y = heart.y

   return heart
}

function playSound() {
   for (var s of sounds)
      s.loop()
}

function genTransitionAnimation() {
   // duration of the transition animation
   const duration = 0.168 // I like golden ratio ;D

   // number of frames needed for the animation
   const stepNum = duration * fps

   // start at (0,0)
   var transition = {x:map.x, y:map.y}

   // track how many frames we've moved
   transition.count = 0

   // animation status
   transition.status = false

   // helper function
   var pos = c => c * map.grid_length + map.grid_length / 2

   transition.draw = function() {
      // current coords, and future coords
      const cx = pos(map.x),        cy = pos(map.y),
            nx = pos(transition.x), ny = pos(transition.y)
      
      const ratio = sqrt(transition.count / stepNum)

      const xPos = lerp(cx, nx, ratio),
            yPos = lerp(cy, ny, ratio)

      map.displayBlob(xPos, yPos)

      transition.count += 1

      // if transition has completed, return to printPlayer
      if (transition.count > stepNum) {
         transition.status = false
         transition.count = 0
         map.x = transition.x
         map.y = transition.y
      }
   }

   return transition
}

function keyPressed() {
   // if we are playing the transition animation, do not response to keypress event
   if (transition.status || scene != mainScene)
      return
   
   switch (keyCode) {
      case UP_ARROW:
         // console.log('up')
         var ny = map.y-1
         if (0<=ny && !map.horiz[map.x][map.y])
            transition.y = ny
      break
      
      case DOWN_ARROW:
         // console.log('down')
         var ny = map.y+1
         if (ny<maze_size && !map.horiz[map.x][ny])
            transition.y = ny
      break

      case LEFT_ARROW:
         // console.log('left')
         var nx = map.x-1
         if (0<=nx && !map.verti[map.x][map.y])
            transition.x = nx
      break

      case RIGHT_ARROW:
         // console.log('right')
         var nx = map.x+1
         if (nx<maze_size && !map.verti[nx][map.y])
            transition.x = nx
      break
   }

   if (map.x != transition.x || map.y != transition.y)
      transition.status = true
}

function genMap(color) {
   // generate maze
   var map = genMaze(maze_size)
   map.color = color;

   // init player coord
   map.x = 0, map.y = 0

   const side_length = min(width, height) * 0.8
   const grid_length = side_length / maze_size
   const wall_width = grid_length * 0.1

   // store this property so that we can use it later
   map.grid_length = grid_length

   // translate distance
   map.tx = abs(side_length - width) / 2
   map.ty = abs(side_length - height) / 2

   // func to draw horizontal wall
   var horWall = (x, y) => {
      fill(map.color, map.alpha)
      rect(x * grid_length, y * grid_length, grid_length, wall_width)
   }

   // func to draw vertical wall
   var verWall = (x, y) => {
      fill(map.color, map.alpha)
      rect(x * grid_length, y * grid_length, wall_width, grid_length)
   }

   map.printMaze = function() {
      for (var i=0; i<map.horiz.length; i++)
         for (var j=0; j<map.horiz[i].length; j++) {
            if (map.horiz[i][j] && i!=map.horiz[i].length-1)
               horWall(i, j)
            if (map.verti[i][j] && j!=map.horiz[i].length-1)
               verWall(i, j)
         }
   }

   map.printPlayer = function() {
      const x = map.x * grid_length + grid_length/2
      const y = map.y * grid_length + grid_length/2

      map.displayBlob(x, y)
   }

   map.displayBlob = function(x, y) {
      // number of layers of circles to be drawn on canvas in order 
      // to emulate focus light effect
      const layers = 80

      noStroke()
      fill(255, 255/(layers+1))

      for (var i = 0; i < layers; i++) {
         ellipse(x, y, grid_length * 1.68 * pow(i/layers, 1.4))
      }
   }

   return map

   // map.updateAlpha = function() {
   //    // if (frameCount % (fps/fps) == 0)
   //       map.alpha = random(0.72*255, 0.8*255)
   //    // console.log(map.alpha)
   // }

   // map.drawAdjacentWalls = function() {
   //    // house keeping consts
   //    const x=map.x, y=map.y

   //    // find left & right wall
   //    var tmp=x
   //    if (map.verti[tmp][y]) {
   //       verWall(tmp, y)
   //       tmp++
   //    }
   //    else {
   //       while(0<=tmp && !map.verti[tmp][y]) tmp--
   //       verWall(tmp, y)
   //       tmp=x
   //    }
   //    while(tmp<map.verti.length && !map.verti[tmp][y]) 
   //       tmp++
   //    verWall(tmp, y)

   //    // find up & down wall
   //    var tmp=y
   //    if (map.horiz[x][tmp]) {
   //       horWall(x, tmp)
   //       tmp++
   //    }
   //    else {
   //       while(0<=tmp && !map.horiz[x][tmp]) tmp--
   //       horWall(x, tmp)
   //       tmp=y
   //    }
   //    while(tmp<map.horiz.length && !map.horiz[x][tmp]) 
   //       tmp++
   //    horWall(x, tmp)

   //    map.updateAlpha()
   // }
}