let dimension
let grid=[], horiz=[], verti=[]

function genMaze(d) {
   dimension = d
   for (var i=0; i<d; i++) grid.push(new Array(d))
   for (var i=0; i<d+1; i++) horiz.push(new Array(d+1))
   for (var i=0; i<d+1; i++) verti.push(new Array(d+1))
   carve_passages_from({x:d-1, y:d-1})
   reverse_maze()
   return {horiz:horiz, verti:verti}
}

dirFuncs = [
   function N(c) {return {x: c.x,   y: c.y-1}},
   function S(c) {return {x: c.x,   y: c.y+1}},
   function W(c) {return {x: c.x-1, y: c.y  }},
   function E(c) {return {x: c.x+1, y: c.y  }}
]

directions = [
   0, 1, 2, 3
]

function carve_passages_from(c) {
   var shuffDir = shuffle(directions)
   for (var d of shuffDir) {
      var n = dirFuncs[d](c)
      if (
         0<=n.x && n.x<dimension &&
         0<=n.y && n.y<dimension &&
         !(grid[n.x][n.y])
      ) {
         grid[c.x][c.y] = true
         grid[n.x][n.y] = true
         switch (d) {
            case 0:
               horiz[c.x][c.y] = true
            break
            case 1:
               horiz[n.x][n.y] = true
            break
            case 2:
               verti[c.x][c.y] = true
            break
            case 3:
               verti[n.x][n.y] = true
            break
         }
         carve_passages_from(n)
      }
   }
}

function reverse_maze() {
   for (var i=0; i<horiz.length; i++)
      for (var j=0; j<horiz[i].length; j++) {
         horiz[i][j] = !horiz[i][j]
         verti[i][j] = !verti[i][j]
      }
}